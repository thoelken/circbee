circBee - Genome-wide analysis of circular RNAs in honeybee reveals a potential correlation to social behavior
===

## Description
This repository documents the RNA-Seq analysis of the article

> manuscript in preparation

If you have comments or suggestions regarding the research, please contact one of the authors.

## Prerequisites
For mapping the reads `segemehl` (tested in version 0.2.0) including `testrealign` is needed:

 - [segemehl website](http://www.bioinf.uni-leipzig.de/Software/segemehl/) ([v0.2.0 download deeplink](http://www.bioinf.uni-leipzig.de/Software/segemehl/segemehl_0_2_0.tar.gz))

Also installing `samtools` (tested in version 1.3.1-36) is recommended for handling SAM and BAM files. One could alternatively sort SAM files using `sort -t $'\t' -k3,3 -k4,4`.

 - [samtools website](http://www.htslib.org/)

All other scripts are written in `Perl`, `Python 3` or `bash`. So best grab a favorite distribution of linux (most ship with the three) or install these on your system.

# Reference Bee Genome
We used the official NCBI reference genome sequences of [Apis mellifera](https://www.ncbi.nlm.nih.gov/genome/?term=Apis%20mellifera):

	for chr in $(seq 16); do wget ftp://ftp.ncbi.nlm.nih.gov/genomes/Apis_mellifera/Assembled_chromosomes/seq/ame_ref_Amel_4.5_chrLG$chr.fa.gz; done
	wget ftp://ftp.ncbi.nlm.nih.gov/genomes/Apis_mellifera/Assembled_chromosomes/seq/ame_ref_Amel_4.5_chrMT.fa.gz
	wget ftp://ftp.ncbi.nlm.nih.gov/genomes/Apis_mellifera/Assembled_chromosomes/seq/ame_ref_Amel_4.5_unplaced.fa.gz

Together with the official annotation of the genome:

	wget ftp://ftp.ncbi.nlm.nih.gov/genomes/Apis_mellifera/GFF/ref_Amel_4.5_top_level.gff3.gz
	zcat ref_Amel_4.5_top_level.gff3.gz amel.gff3

Sequence identifiers in the FASTAs do not match the GFF3 chromosome names and are changed accordingly:

	zcat ame_ref_Amel_4.5_*.fa.gz | sed -r 's/^>.*(NC_[0-9\.]+)\|.*/>\1/' > amel.mfa

## Processing of RNA-Seq Data
We constructed 4 different libraries:

| SRA ID 		| name 		| description 																	|
| :---: 			| :---: 		| :--- 																			|
| SRR4343845 	| Rminus 	| pooled nurse and forager bee brains without treatment 			|
| SRR4343846 	| Rplus 		| pooled nurse and forager bee brains with RNase R treatment 	|
| SRR4343847 	| nurse 		| nurse bee brains with RNase R treatment 							|
| SRR4343848 	| forager 	| forager bee brains with RNase R treatment 							|

The first two are single-end sequenced while the latter two are paired-end.
Inspection with `fastqc` revealed sufficient read quality for direct mapping (entire anlysis was also done with strict quality trimming without changing the results significantly).

Before we can map the reads, we need to build a `segemehl` suffix tree / index ([segemehl quick start](http://www.bioinf.uni-leipzig.de/Software/segemehl/)):

	segemehl -x amel.mfa.idx -d amel.mfa

Now we can map the single-end reads:

	segemehl --index amel.mfa.idx --database amel.mfa --query Rminus.fq.gz --silent --splits --evalue 0.1 --threads 4 | samtools view -buSh - | samtools sort - | samtools view -h - > Rminus.sam
	segemehl --index amel.mfa.idx --database amel.mfa --query Rplus.fq.gz --silent --splits --evalue 0.1 --threads 4 | samtools view -buSh - | samtools sort - | samtools view -h - > Rplus.sam
	segemehl --index amel.mfa.idx --database amel.mfa --query nurse_1.fq.gz --mate nurse_2.fq.gz --silent --splits --evalue 0.1 --threads 4 | samtools view -buSh - | samtools sort - | samtools view -h - > nurse.sam
	segemehl --index amel.mfa.idx --database amel.mfa --query forager_1.fq.gz --mate forager_2.fq.gz --silent --splits --evalue 0.1 --threads 4 | samtools view -buSh - | samtools sort - | samtools view -h - > forager.sam

We can assess some mapping statistics:

	TODO!

## Investigation of Circular Junctions
For each dataset we determine all possible splice events based on segemented reads mapping to different sections of the reference using `testrealign`

	testrealign --database amel.mfa --query Rminus.sam --norealign --threads 4 --splitfile Rminus.split.bed --transfile Rminus.trans.bed
	testrealign --database amel.mfa --query Rplus.sam --norealign --threads 4 --splitfile Rplus.split.bed --transfile Rplus.trans.bed
	testrealign --database amel.mfa --query nurse.sam --norealign --threads 4 --splitfile nurse.split.bed --transfile nurse.trans.bed
	testrealign --database amel.mfa --query forager.sam --norealign --threads 4 --splitfile forager.split.bed --transfile forager.trans.bed

These files contain also "conventional" splice sites and trans-splice sites. We filter only the putatively circular and integrate all junctions with at least a total of 10 junction spanning reads with `circquant.py`:

	python3 circquant.py --min_circs 10 Rminus.split.bed Rplus.split.bed nurse.split.bed forager.split.bed > circbee.bed

Lets also find out which genes and exact exons they belong to:

	perl feature_region.pl amel.gff3 < circbee.bed > circbee_features.tsv

This data is tabular and can easily be filtered and analyzed further in `LibreOffice Calc` or `Excel`.


## Linking circRNA in Drosophila from circbase
### Orthology mapping by OrthoDB
First, we need good orthology assignments between fly and bee that we get from `OrthoDB`:

	wget http://www.orthodb.org/v9/download/odb9_OG2genes.tab.gz 	# Orthologous Groups mapped to ODB gene IDs
	wget http://www.orthodb.org/v9/download/odb9_genes.tab.gz 		# ODB gene IDs mapped their more common IDs
	wget http://www.orthodb.org/v9/download/odb9_species.tab.gz 	# taxonomy IDs mapped to actual species names

We leave these compressed and extract only the needed information for bee and fly:

	zgrep mellifera odb9_species.tab.gz 		# the TaxID is 7460 for bee
	zgrep melanogaster odb9_species.tab.gz 	# the TaxID is 7227 for fly
	zgrep 7460: odb9_genes.tab.gz > odb9_bee_genes.tsv 	# 15,314 genes
	zgrep 7227: odb9_genes.tab.gz > odb9_fly_genes.tsv 	# 13,920 genes
	zgrep 7460: odb9_OG2genes.tab.gz > odb9_bee_ogs.tsv 	# 93,631 entries
	zgrep 7227: odb9_OG2genes.tab.gz > odb9_fly_ogs.tsv 	# 95,152 entries

Let's join these each:

	join -t$'\t' -1 2 -2 1 <(sort -k2,2 odb9_bee_ogs.tsv) odb9_bee_genes.tsv | cut -f1-5 | sort -k4,4 > odb9_bee.tsv
	join -t$'\t' -1 2 -2 1 <(sort -k2,2 odb9_fly_ogs.tsv) odb9_fly_genes.tsv | cut -f1-5 | sort -k4,4 > odb9_fly.tsv


### Re-annotating circbase junctions
To compare our candidates to circRNA previously identified, the circbase.org table and the corresponding genome version has to be downloaded:

Go to circbase.org -> table browser -> select Organism: "D.melanogaster dm3" Study:"Ashwal 2014" -> Search -> download results under Export results: "txt". The result is a tab-separated file called [circBase_export.txt](gitlab.com/thoelken/circbee/circBase_export.txt)'.

The 'best transcript' and 'gene symbol' columns of this table are already a great start to identify circRNAs of orthologous origin. However, these are not necessarily the exact loci covering the reported junctions, but rather ne nearest gene. So, a safer method to determining orthologous origin is to completely re-annotate these junctions.

Reformat this file to resemble the BED format:

	sed -rn 's/^chr(.+):([0-9]+)-([0-9]+)\s([-\+])/\1\t\2\t\3\tcircbase\t0\t\4/p' circBase_export.txt > circbase.bed

For re-annotation we also need the corresponding genome version for the the Ashwal2014 dataset. The authors used `dm3` as the genome version and included some transcript IDs that have not been used in the last 5 years or so. They are latest part of the assembly version 5.12 of FlyBase from late 2008 and coordinates seem to also match this assembly.

	wget ftp://ftp.flybase.net/genomes/Drosophila_melanogaster/dmel_r5.12_FB2008_09/gff/dmel-all-r5.12.gff.gz

As before with our `circ_junct.all.bed` candidates, we can annoated `circbase.bed` with this `dmel_r512.gff`:

	perl feature_region.pl <(zcat dmel-all-r5.12.gff.gz) < circbase.bed | cut -f7,17 | awk '{print $2"\t"$1}' | sort -k1,1 > circbase_fb2id.tsv


### Joining the two datasets (based on orthologous origin)
OrthoDB only knows the BeeBase ID of bee genes, so we first have to map our gene IDs to BeeBase:

	sed -rn 's/^.*ID=([^;,:.]+)[;,:.].*BEEBASE:([^;,.:]+)[;,.:].*$/\2\t\1/p' amel.gff3 | sort -k1,1 > bee_gb2id.tsv
	join -t$'\t' -2 4 bee_gb2id.tsv odb9_bee.tsv | sort -k3,3 > bee_id2og.tsv

For each circbase junction assign the orthologous group if possible:

	join -t$'\t' -2 4 circbase_fb2id.tsv odb9_fly.tsv | sort -k2,2 > circbase_ogs.tsv 	# ambiguous associations are possible because one gene might belong to multiple OGs

Match all bee genes to circbase where the fly gene is considered orthologous:

	join bee_id2og.tsv circbase_ogs.tsv > intersect.tsv

Finally, annotate our results with this information:

	join -a1 circbee_features.tsv intersect.tsv > circbee_circbase.tsv

