#!/usr/bin/env python3

import sys
import re
from argparse import ArgumentParser
from collections import defaultdict


verbose = False

cli = ArgumentParser(description=("A tool to identify backspliced "
                                  "sequences from segemehl SAM alignments."))
cli.add_argument('-d', '--dist-min', type=int, default=50, metavar='N',
                 help="min distance of junction (transcript specific if used with -g)")
cli.add_argument('-D', '--dist-max', type=int, default=2000, metavar='N',
                 help="max distance of junction (transcript specific if used with -g)")
cli.add_argument('-v', '--verbose', action='store_true',
                 help="show verbose output to STDERR")
cli.add_argument('-n', '--count-min', type=int, default=1, metavar='N',
                 help="min count of junctions for output")
cli.add_argument('-o', '--output-prefix', metavar='PREFIX',
                 help="prefix for split SAM output files")
cli.add_argument('-g', '--gff', type=open, metavar='FILE',
                 help="annotation file in GTF/GFF3 format")
cli.add_argument('-s', '--sam', type=open, metavar='FILE',
                 help="SAM input file")


# helper class for writing to null when no file is specified
class EmptyWriter(object):
    def write(self, line):
        return

    def close(self):
        return


# read sam and return tuple of header, split read starts and ends
def split_sam(samfile):
    # 1 read_id, 2 flag, 3 chro, 4 r_start, 5 s_start,
    # 6 s_end, 7 s_num, 8 other_chro, 9 other_r_start
    pattern = re.compile(r'^([^\t]+)\t(\d+)\t([^\s]+)\t(\d+).*XX:i:(\d+)\s+'
                         r'XY:i:(\d+)\sXQ:i:(\d)\sX[CP]:Z:([^\s]+)\s'
                         r'X[VU]:i:(\d+)\s*.*$')
    starts = dict()
    ends = dict()
    header = ""
    for l in samfile:
        if l.startswith("@"):
            header += l
            next
        m = pattern.match(l)
        if m:
            read_id = m.group(1)
            flag = int(m.group(2))
            rev_strand = flag & 16
            chro = m.group(3)
            r_start = int(m.group(4))
            s_num = m.group(7)
            other_chro = m.group(8)
            other_r_start = int(m.group(9))
            s_num = m.group(7)

            # anti-sense alignments need start/end swap
            if rev_strand:
                r_start, other_r_start = (other_r_start, r_start)

            # ignore mappings to different chromosomes
            if chro != other_chro:
                next

            # if first segment, next should be mapped upstream in reference
            # vice versa for second segment and anti-sense alignments resp.
            if s_num is "0" and other_r_start < r_start:
                starts[read_id] = l
            elif s_num is "1" and r_start < other_r_start:
                ends[read_id] = l
    return (header, starts, ends)


# takes starts and ends of split reads and matches them based on same chromosome, strand and
# exactly adjacent positions
def match_splits(header, starts, ends, out_prefix=None):
    junctions = defaultdict(int)
    # 1 flag, 2 chromosome, 3 ref_start, 4 segment_start, 5 segment_end
    pattern = re.compile(r'^[^\t]+\t+(\d+)\t+([^\s]+)\t(\d+)\s.+\s+XX:i:(\d+)\s+XY:i:(\d+).*')

    # open two SAM files and write junction starts and ends separately
    if out_prefix is None:  # substitude with dummy if not writing to file
        first = EmptyWriter()
        second = first
    else:
        first = open(out_prefix+"_0.sam", 'w')
        second = open(out_prefix+"_1.sam", 'w')

    first.write(header)
    second.write(header)

    small_gaps = 0
    for k1 in starts:
        if k1 in ends:
            m1 = pattern.match(starts[k1])
            m2 = pattern.match(ends[k1])
            if m1 and m2:   # check for valid SAM format
                strand1 = int(m1.group(1)) & 16     # true if minus strand?
                strand2 = int(m2.group(1)) & 16
                chr1 = m1.group(2)
                chr2 = m2.group(2)
                r1_start = int(m1.group(3))
                r2_start = int(m2.group(3))
                s1_start = int(m1.group(4))
                s2_start = int(m2.group(4))
                s1_end = int(m1.group(5))
                s2_end = int(m2.group(5))

                start = r2_start
                end = r1_start + s1_end-s1_start
                strand = '+'
                # assuming a read: >-seg1-||-seg2->
                #                 /      /  \      \
                #            start    end    start  end
                # mapping to +strand: |-seg2->...>-seg1-|
                # mapping to -strand: |-1ges-<...<-2ges-|
                if strand1:     # should be the other way around, output is correct though
                    start = r1_start
                    end = r2_start + s2_end-s2_start
                    strand = '-'

                if 1 < s2_start - s1_end <= 3:
                    small_gaps += 1

                if strand1 == strand2 and chr1 == chr2 and s1_end+1 == s2_start:
                    first.write(starts[k1])
                    second.write(ends[k1])
                    junctions["%s\t%d\t%d\tcircjunct\t0\t%s" % (chr1, start, end, strand)] += 1
        else:
            if verbose:
                sys.stderr.write("No matching segment in data: %s\n" % k1)

    first.close()
    second.close()
    sys.stderr.write("there were %d junction reads with just a small gap" % small_gaps)
    return junctions


# read GFF and print only junctions which really map to exonic junctions
def filter_exonic_junct(junctions, gff, min_dist=50, max_dist=2000, echo=False):
    ret = dict()
    exonic_starts = defaultdict(lambda: defaultdict(dict))
    exonic_ends = defaultdict(lambda: defaultdict(dict))
    exonic_lengths = defaultdict(lambda: defaultdict(dict))
    # 1 chr, 2 origin, 3 type, 4 start, 5 end, 6 strand, 7 id
    p = re.compile(r'(\S+)\s(\S+)\s(\S+)\s(\d+)\s(\d+)\s.+\s(\+|-)\s.+\s.*?ID=([^;]+);.*')
    # 1 chr, 2 start, 3 end, 4 strand
    pj = re.compile(r'(\S+)\s(\d+)\s(\d+)\scircjunct\s0\s(\+|-).*')
    for l in gff:
        m = p.match(l)
        if m and m.group(3) == "exon":
            chro = m.group(1)
            strand = m.group(6)
            start = int(m.group(4))
            end = int(m.group(5))
            name = m.group(7)
            exonic_starts[chro][strand][start] = name
            exonic_ends[chro][strand][end] = name
            exonic_lengths[chro][strand][start] = end-start

    for j in junctions:
        m = pj.match(j)
        if m:
            chro = m.group(1)
            start = int(m.group(2))
            end = int(m.group(3))
            strand = m.group(4)
            es = exonic_starts[chro][strand]
            ee = exonic_ends[chro][strand]
            if start in es and end in ee:
                el = exonic_lengths[chro][strand]
                # sum length of all exons between junction
                length = sum([el[i] for i in el if start <= i and i+el[i] <= end])
                if min_dist <= length <= max_dist:
                    ret[j] = (junctions[j], es[start], ee[end])
                    if echo:
                        print("%s\t%d\t%s\t%s\t%d" % (j, junctions[j], es[start], ee[end], length))
    return ret


def print_junctions(junctions, min_counts=1):
    for k in junctions:
        if min_counts <= junctions[k]:
            print("%s\t%d" % (k, junctions[k]))


def main():
    global verbose

    args = cli.parse_args()
    verbose = args.verbose

    if args.sam:
        header, starts, ends = split_sam(args.sam)
        args.sam.close()
    else:
        sys.stderr.write("Reading SAM alignments from STDIN:")
        sys.stderr.flush()
        header, starts, ends = split_sam(sys.stdin)
        sys.stderr.write("\r                                  \r")

    junctions = match_splits(header, starts, ends, args.output_prefix)
    if args.gff:
        filter_exonic_junct(junctions, args.gff, args.dist_min, args.dist_max, True)
        args.gff.close()
    else:
        print_junctions(junctions, args.count_min)


if __name__ == '__main__':
    main()
