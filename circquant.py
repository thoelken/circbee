#!/usr/bin/env python3

from collections import defaultdict
from argparse import ArgumentParser as AP


cli = AP(description='''Quantification of circRNA-Seq mapping results
Reads splice information from segemehls testrealign or circumjunct to quantify
circular junctions against their linear splicing counterparts and outputs a table
of all circular junctions found across multiple samples.''')

cli.add_argument('bedfiles', nargs='+', help='multiple BED files '
                 'from containing splice sites')
cli.add_argument('-m', '--min_circs', type=int, default=1,
                 help='minimum number of found circular junctions in total')
cli.add_argument('-s', '--min_samples', type=int, default=1,
                 help='minimum number of samples containing a circular junction')
cli.add_argument('-i', '--min_insert', type=int, default=100,
                 help='minimal insert size between junctions (default=100)')
cli.add_argument('-I', '--max_insert', type=int, default=10000,
                 help='maximal insert size between junctions (default=10000)')


if __name__ == '__main__':
    args = cli.parse_args()
    alljunct = {}   # all circ junctions across all files
    linear = {}     # linear splice junctions per file
    circular = {}   # circular junctions per file
    for f in args.bedfiles:
        lin = defaultdict(int)
        circ = {}
        with open(f, 'r') as lines:
            for l in lines:
                cells = l.split()
                chro = cells[0].replace('|', '#')   # chromosome
                start = int(cells[1])
                end = int(cells[2])
                splits = cells[3].split(':')    # splits:12:1:4:C
                typ = splits[0]                 # splicing indicator
                circsplit = int(splits[1])      # number of circular junctions
                firstsplit = int(splits[2])     # number of other reads at first pos
                lastsplit = int(splits[3])      # number of other reads at second pos
                flag = splits[4]                # either circular 'C' or normal/linear 'N'
                passed = splits[5]
                strand = cells[5].rstrip()
                if typ == 'splits' and flag == 'C' and passed in ['P', 'F']\
                        and args.min_insert <= end-start <= args.max_insert:
                    key = "%s|%d|%c|%d" % (chro, start, strand, end)
                    ratio = 1.0*circsplit/(firstsplit+lastsplit)*2  # ratio of junction reads
                    alljunct[key] = 1
                    circ[key] = [ratio, circsplit]
                if typ == 'splits' and flag == 'N':
                    lin["%s|%d|%c" % (chro, start, strand)] += circsplit
                    lin["%s|%d|%c" % (chro, end, strand)] += circsplit
        linear[f] = lin
        circular[f] = circ

    print("#chromosome\tstart\tend\tcirc\tscore\tstrand\t%s\t#total_circjunct\t#total_linear" %
          '\t'.join(["%s_ratio\t#circjunct\t#linear" % f for f in args.bedfiles]))
    for k in sorted(alljunct):
        total_circ = 0
        sample_circ = 0
        total_linear = 0
        (chro, start, strand, end) = k.split('|')
        text = "%s\t%d\t%d\tcirc\t0\t%c" % (chro, int(start), int(end), strand)
        s = "%s|%d|%c" % (chro, int(start), strand)
        e = "%s|%d|%c" % (chro, int(end), strand)
        for f in args.bedfiles:
            count = 0
            ratio = 0.0
            if k in circular[f]:
                (ratio, count) = circular[f][k]
                total_circ += count
                sample_circ += 1
            total_linear += int(linear[f][s]+linear[f][e])
            text += "\t%f\t%d\t%d" % (ratio, count, int(linear[f][s]+linear[f][e]))
        text += "\t%d\t%d" % (total_circ, total_linear)
        if total_circ >= args.min_circs and sample_circ >= args.min_samples:
            print(text)
