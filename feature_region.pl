#!/usr/bin/perl

use warnings;
use strict;

if(@ARGV ne 1) {
	print STDERR "usage: feature_region annotation.gff < regions.bed\n";
	exit 1;
}
my $gff_name = $ARGV[0];

my (%genes, %exons, $last_gene);
open(my $gff, '<', $gff_name);
while (<$gff>) {
	# NC_000964.3 	refseq 	gene 	123 	232 	0 	+ 	0 	ID=gene1232;stuff=sdfds
	if ( $_ =~ qr/^(\S+)\s\S+\s(\S+)\s(\d+)\s(\d+)\s\S\s([-\+])\s\S\s.*ID=([^;]+);(.*)$/) {
		if ( $2 eq 'gene' ) {
			$genes{$1}{$3} = [$1, $2, $3, $4, $5, $6];
			$last_gene = $6;
			my $chro = $1; my $start = $3;
			# if ($7 =~ qr/Name=([^;]+);?/) { $genes{$chro}{$start}[5] = $1; $last_gene = $1 }
		} elsif ( $2 eq 'exon' ) {
			$exons{$last_gene}{$3} = [$1, $2, $3, $4, $5, $6];
		}
	}
}
close($gff);

my $first = 1;
while(<STDIN>) {
	chomp();
	# NC_007079.3     123222  124872  splits:22:23:32:C:P     0       +
	if( $_ =~ /^(\S+)\s(\d+)\s(\d+)\s\S+\t\d+\s([-\+]).*$/) {
		my $chr = $1;
		my $start = $2;
		my $end = $3;
		my $strand = $4;
		my $start_g = 'intergenic'; my $end_g = 'intergenic';
		my $start_e = 'intergenic'; my $end_e = 'intergenic';
		foreach my $a_l (sort keys %{$genes{$chr}}) {
			my @g = @{$genes{$chr}{$a_l}};
			if ($g[1] eq "") {
				next;
			} elsif ($g[4] ne $strand) {
				# next;
			}
			if($g[2] <= $start && $start <= $g[3]) {
				$start_g = $g[5];
				$start_e = 'intronic'; $end_e = 'intronic';
				foreach my $b_l (sort keys %{$exons{$g[5]}}) {
					my @e = @{$exons{$g[5]}{$b_l}};
					if ($e[2] <= $start && $start <= $e[3]) {
						$start_e = $e[5];
						last;
					}
				}
			}
			if($g[2] <= $end && $end <= $g[3]) {
				$end_g = $g[5];
				foreach my $b_l (sort keys %{$exons{$g[5]}}) {
					my @e = @{$exons{$g[5]}{$b_l}};
					if ($e[2] <= $end && $end <= $e[3]) {
						$end_e = $e[5];
						last;
					}
				}
			}
			if($start_g ne 'intergenic' && $end_g ne 'intergenic') {
				last;
			}
		}
		print $_."\t".$start_g."\t".$end_g."\t".$start_e."\t".$end_e."\n";
	} elsif($first == 1) {
		print $_."\t5prime_gene\t3prime_gene\t5prime_exon\t3prime_exon\n";
		$first = 0;
	}
}
